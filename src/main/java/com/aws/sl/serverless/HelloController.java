package com.aws.sl.serverless;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RestController
@EnableWebMvc
public class HelloController {

    @GetMapping("/hello")
    public String sayHello(){
        return "Running serverless from SpringBoot 3 in AWS.";
    }
}
