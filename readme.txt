Install AWS CLI and SAM CLI

Run following commands to deploy serverless app

1. sam build --use-container
2. sam deploy --guided --profile dev

If profile is not found, create profile using AWS CLI.

aws configure --profile dev
